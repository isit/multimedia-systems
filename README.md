# isit-responsive

> Study

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

### Front end frameworks
0. https://github.com/usablica/front-end-frameworks
0. https://github.com/topics/css-framework
0. https://autoprefixer.github.io/ru/


### Содержание записки
- 4 страницы теория
- 4 страницы используемые стредства
- 6-8 страниц последовательность разработки

### Information
- https://developer.mozilla.org/ru/docs/Web/Guide/CSS/Getting_started/What_is_CSS


For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
