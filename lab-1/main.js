//база знаний
const knowledge = [
  ["css",
    "является",
    "формальным языком описания внешнего вида документа, написанного с использованием языка разметки"],
  ["цсс",
    "является",
    "формальным языком описания внешнего вида документа, написанного с использованием языка разметки"],
  ["bootstrap",
    "состоит",
    "из наборы CSS стилей и JavaScript плагинов"],
  ["html",
    "состоит",
    "как минимум из трёх тегов: html, head и body"],
  ["система сеток",
    "состоит",
    "из «12-ти колонок», 5-ти адаптивных ярусов, препроцессоров Sass и десятков предустановленных классов"],
];
//текст сообщений
const message1 = "Вы слишком долго смотрите текущую страницу. " +
  "Что-нибудь не понятно?<br>" +
  "<input id='Q1' placeholder='Введите вопрос'/><br>" +
  "<button onclick='ask(\"Q1\")'>Спросить</button>";
const message2 = "Вы слишком быстро листаете страницы. Что-нибудь не понятно?<br>" +
  "<input id='Q2' placeholder='Введите вопрос'/><br>" +
  "<button onclick='ask(\"Q2\")'>Спросить</button>";
let timer; 		//таймер
let timeout; 	//время до появления медального окна 1, зависящее от размера страницы
let dialogOn = false;
let textLine;

//подготовка среды – эту функцию нужно привязать к событию onload тега body
function prepare_environment() {
  //окна активной среды
  timeout = document.body.innerHTML.length;
  document.body.innerHTML += "<div id='alert1' style='display:none'>" +
    "<div class='bg' onclick='hide(\"alert1\")'>&nbsp;</div>" +
    "<div class='alert_message'>" + message1 + "</div>" +
    "</div>";
  document.body.innerHTML += "<div id='alert2' style='display:none'>" +
    "<div class='bg' onclick='hide(\"alert2\")'>&nbsp;</div>" +
    "<div class='alert_message'>" + message2 + "</div>" +
    "</div>";
  //диалоговый модуль
  document.body.innerHTML +=
    "<div id='dialog' class='dialog' style='margin-left:-25px;'>" +
    "<div class='label' onclick='toggleDialog()'>Нажми, чтобы спросить!</div>" +
    "<div class='header'>История:</div><div class='history' id='history'></div>" +
    "<div class='question'><input id='QDialog' placeholder='Введите вопрос'/><br>" +
    "<button onclick='ask(\"QDialog\")'>Спросить</button>" +
    "</div></div>";
  //привязка окон активной среды с событиями
  //показ модального окна 1 через интервал времени, зависящий от размера страницы
  timer = setInterval(alert_over_time, timeout);

  let UrlLog, log;
  try {
    //открытие журналов посещенных адресов и дат посещения:
    //попытка использования массива адресов открытых страниц из локального хранилища
    UrlLog = JSON.parse(localStorage.URLlog);
    //удаление адресов из начала массива адресов, пока не останется 5 адресов
    while (UrlLog.length > 5) UrlLog.shift(0);
    //попытка использования массива дат открытия страниц из локального хранилища
    log = JSON.parse(localStorage.log);
    //удаление дат из начала массива, пока в массиве не останется 5 дат
    while (log.length > 5) log.shift(0);
    //проверка на необходимость срабатывания реакции:
    //только если сделан переход со страницы на страницу (не обновление страницы)
    if (location.href !== UrlLog[UrlLog.length - 1]) {
      //если сделано 5 переходов меньше чем за минуту -
      // очищаем массив и показываем модальное окно 2
      if (log.length >= 5 && ((new Date()) - Date.parse(log[0])) < 60000) {
        while (log.length > 0) log.shift(0); //очистка массива дат
        alert_for_speed();
      }
      //в любом случае, независимо от срабатывания реакций,
      // при переходе со страницы на страницу:
      UrlLog.push(location.href);	//запись адреса текущей страницы в массив
      log.push(new Date());				//запись даты перехода в массив
    }
  }
  catch (e) {
    UrlLog = [];	//инициализация массива адресов открытых страниц
    log = [];		//инициализация массива дат открытия страниц
  }
  //запись массива адресов в локальное хранилище в формате JSON
  localStorage.URLlog = JSON.stringify(UrlLog);
  //запись массива дат в локальное хранилище в формате JSON
  localStorage.log = JSON.stringify(log);

  // Задаем API-ключ
  window.ya.speechkit.settings.apikey = "44cdbdb5-ebab-4b8a-8669-c65c826a5fc8";
  window.ya.speechkit.settings.lang = "ru-RU";

  // Добавление элемента управления "Поле для голосового ввода".
  textLine = new ya.speechkit.Textline("QDialog", {
    onInputFinished: function(text) {
      // Финальный текст.
      console.log(text);
      ask("QDialog");
    }
  });
}

//скрытие окон сообщений
function hide(elem_id) {
  $("#" + elem_id).css({"display": "none"});
  timer = setInterval(alert_over_time, timeout);
}

//показ сообщений
function alert_over_time() {
  $("#alert1").css({"display": "block"});
  clearInterval(timer);
}

function alert_for_speed() {
  $("#alert2").css({"display": "block"});
  clearInterval(timer);
}

//ДИАЛОГ
//показ-скрытие диалогового модуля
function toggleDialog() {
  if (dialogOn) {		//закрытие
    $("#dialog").animate({"margin-left": "-25px"}, 1000, function () {
    });
    dialogOn = false;
    timer = setInterval(alert_over_time, timeout);
  }
  else {					//открытие
    $("#dialog").animate({"margin-left": "-300px"}, 1000, function () {
    });
    dialogOn = true;
    clearInterval(timer);
  }
}

//поиск и вывод ответа и вопроса
function ask(questionInput) {
  let question = document.getElementById(questionInput).value.trim();
  //выдвижение диалогового модуля важно для вопросов из модального окна
  $("#dialog").animate({"margin-left": "-300px"}, 1000, function () {
  });
  dialogOn = true;
  //вывод вопроса - создаем блок <div>
  let newDiv = document.createElement("div");
  newDiv.className = 'question'; 	//задаем класс оформления созданного блока
  newDiv.innerHTML = question; 		//наполняем блок текстом вопроса
  document.getElementById("history").appendChild(newDiv);
  //поиск и вывод ответа
  newDiv = document.createElement("div");
  newDiv.className = 'answer';
  //получаем ответ на вопрос и наполняем им созданный блок
  newDiv.innerHTML = getAnswer(question);
  //ОЗВУЧКА - СИНТЕЗ РЕЧИ
  let needSound = true; 		//флаг, нужна ли озвучка (не нужна, если есть анимация)
  //проходим по элементам HTML-кода ответа
  for (let i = 0; i < newDiv.childNodes.length; i++)
    //если находим элемент <embed>
    if (newDiv.childNodes[i].tagName === "EMBED") {
      //сбрасываем флаг и выходим из цикла
      needSound = false;
      break;
    }
  if (needSound) {			//если флаг не был сброшен
    //добавляем в ответ тег аудио, ссылающийся на звук от синтезатора речи яндекса
    //в обращении к яндексу tts.voicetech.yandex.net указывается:
    // - формат звука: format=mp3		 - язык озвучиваемого текста: lang=ru-RU
    // - ключ, полученный при регистрации в личном кабинете
    //   для SpeechKit Cloud API: key=4a4d3a13-d206-45fc-b8fb-e5a562c9f587
    // - озвучиваемый текст, который берется из сгенерированного ответа:
    //   text="+(newDiv.innerText||newDiv.textContent)+"
    newDiv.innerHTML += "<audio controls='true' autoplay='true' " +
      "src='http://tts.voicetech.yandex.net/generate?format=mp3&" +
      "lang=ru-RU&key=4a4d3a13-d206-45fc-b8fb-e5a562c9f587&" +
      "text=" + (newDiv.innerText || newDiv.textContent) + "'></audio>";
  }
  // КОНЕЦ ОЗВУЧКИ
  document.getElementById("history").appendChild(newDiv);
  // ЕЩЕ КУСОЧЕК ДЛЯ ОЗВУЧКИ - запуск звука
  if (newDiv.lastChild["tagName"] === "AUDIO") newDiv.lastChild.play();
  //прокрутка истории в самый низ
  document.getElementById("history").scrollTop =
    document.getElementById("history").scrollHeight;
  //очистка текстового поля для ввода вопроса
  document.getElementById(questionInput).value = "";
}

//псевдоокончания сказуемых (глаголов, кратких причастий и прилагательных)
let endings = [
  ["ет", "(ет|ут|ют)"], ["ут", "(ет|ут|ют)"], ["ют", "(ет|ут|ют)"],//1 спряжение
  ["ит", "(ит|ат|ят)"], ["ат", "(ит|ат|ят)"], ["ят", "(ит|ат|ят)"],//2 спряжение
  ["ется", "(ет|ут|ют)ся"], ["утся", "(ет|ут|ют)ся"],
  ["ются", "(ет|ут|ют)ся"],//1 спряжение, возвратные
  ["ится", "(ит|ат|ят)ся"], ["атся", "(ит|ат|ят)ся"],
  ["ятся", "(ит|ат|ят)ся"],//2 спряжение, возвратные
  ["ен", "ен"], ["ена", "ена"], ["ено", "ено"], ["ены", "ены"],//кр. прилагательные
  ["ан", "ан"], ["ана", "ана"], ["ано", "ано"], ["аны", "аны"],//кр. прилагательные
  ["жен", "жен"], ["жна", "жна"], ["жно", "жно"], ["жны", "жны"],//кр. прилагательные
  ["такое", "- это"]];//для вопроса "что такое А?" ответ - "А - это ..."
//черный список слов, распознаваемых как сказуемые по ошибке
let blacklist = ["замена", "замены", "атрибут", "маршрут", "член", "нет"];

//функция определения сказуемых по соответствующим псевдоокончаниям
function getEnding(word) {
  if (blacklist.indexOf(word) !== -1) return -1; 		//проверка по черному списку
  for (let j = 0; j < endings.length; j++)					//перебор псевдоокончаний
    //проверка, оканчивается ли i-ое слово на j-ое псевдоокончание
    if (word.substring(word.length - endings[j][0].length) === endings[j][0])
      return j;	 //возврат номера псевдоокончания
  return -1;	//если совпадений нет - возврат -1
}

//маленькая функция, которая делает первую букву переданной строки большой
function big1(str) {
  return str.substring(0, 1).toUpperCase() + str.substring(1);
}

function getAnswer(txt) {			//главная функция, ищущая ответ на вопрос
  console.log(txt);
  let separators = "'\",.!?()[]\\/";			//знаки препинания
  //добавление пробелов перед знаками препинания
  for (let i = 0; i < separators.length; i++)
    txt = txt.replace(separators[i], " " + separators[i]);
  let words = txt.split(' '); 		//массив слов и знаков препинания
  let result = false; 					//флаг, найден ли ответ
  let answer = "";								//формируемый функцией ответ на вопрос
  //перебор слов
  for (let i = 0; i < words.length; i++) {
    //поиск номера псевдоокончания
    let ending = getEnding(words[i]);
    //если псевдоокончание найдено – это сказуемое,
    // подлежащее в вопросе после него
    if (ending >= 0) {
      //---ТОЧНЫЙ ПОИСК---
      let subject_array = words.slice(i + 1);
      let subject_text = subject_array.join(" ");
      console.log(subject_text);
      console.log('--------------');
      for (let j = 0; j < knowledge.length; j++) {
        // console.log(words[i], knowledge[j][1]);
        // console.log(subject_text, knowledge[j][0]);
        if ((words[i] === knowledge[j][1]) &&
          (subject_text === knowledge[j][0]) || (subject_text === knowledge[j][2])) {
          //создание простого предложения из семантической связи
          answer += big1(knowledge[j][0] + " " +
            knowledge[j][1] + " " + knowledge[j][2] + ". ");
          result = true;
        }
      }
      //---ПОИСК С ПОМОЩЬЮ РЕГУЛЯРНЫХ ВЫРАЖЕНИЙ---
      if (result === false) {
        //замена псевдоокончания на набор возможных окончаний
        words[i] = words[i].substring(0, words[i].length -
          endings[ending][0].length) + endings[ending][1];
        //создание регулярного выражения для поиска по сказуемому из вопроса
        let predicate = new RegExp(words[i]);
        //для кратких прилагательных захватываем следующее слово
        if (endings[ending][0] === endings[ending][1]) {
          predicate = new RegExp(words[i] + " " + words[i + 1]);
          i++;
        }
        //создание регулярного выражения для поиска по подлежащему из вопроса
        //из слов подлежащего выбрасываем короткие предлоги
        // (периметр у квадрата = периметр квадрата)
        for (let j = 0; j < subject_array.length; j++)
          if (subject_array[j].length < 3) {
            subject_array.splice(j);
            j--;
          }
        let subject_string = subject_array.join(".*");
        //только если в подлежащем больше трех символов
        if (subject_string.length > 3) {
          let subject = new RegExp(".*" + subject_string + ".*");
          //поиск совпадений с шаблонами среди связей семантической сети
          for (let j = 0; j < knowledge.length; j++)
            if (predicate.test(knowledge[j][1]) &&
              (subject.test(knowledge[j][0]) || subject.test(knowledge[j][2]))) {
              //создание простого предложения из семантической связи
              answer += big1(knowledge[j][0] + " " +
                knowledge[j][1] + " " + knowledge[j][2] + ". ");
              result = true;
            }
          //если совпадений с двумя шаблонами нет,
          if (result === false)
          //поиск совпадений только с шаблоном подлежащего
            for (let j = 0; j < knowledge.length; j++)
              if (subject.test(knowledge[j][0]) || subject.test(knowledge[j][2])) {
                //создание простого предложения из семантической связи
                answer += big1(knowledge[j][0] + " " +
                  knowledge[j][1] + " " + knowledge[j][2] + ". ");
                result = true;
              }
        }
      }
    }
  }
  //если ответа не нашли – так и пишем
  if (!result) answer = "Ответ не найден. <br/>";
  return answer;
}

$(window).load(function() {
  prepare_environment();
});
