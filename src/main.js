import 'bootstrap'

import Vue from 'vue'

import App from './App.vue'
new Vue({
  el: '#app',
  render: h => h(App)
});

import Bootstrap from './Bootstrap.vue'
new Vue({
  el: '#bootstrap',
  render: h => h(Bootstrap)
});

import Skeleton from './Skeleton.vue'
new Vue({
  el: '#skeleton',
  render: h => h(Skeleton)
});

import Bulma from './Bulma.vue'
new Vue({
  el: '#bulma',
  render: h => h(Bulma)
});

import SemanticUI from './SemanticUI.vue'
new Vue({
  el: '#semantic-ui',
  render: h => h(SemanticUI)
});
